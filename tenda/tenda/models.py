from django.db import models

# Create your models here.

class Tenda(models.Model):
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=100)
    adreça = models.CharField(max_length=200)

    def __str__(self):
        return self.nom


class Marca(models.Model):
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=100)

    def __str__(self):
        return self.nom


class Producte(models.Model):
    id = models.AutoField(primary_key=True)
    nom = models.CharField(max_length=100)
    preu_unitari = models.DecimalField(max_digits=10, decimal_places=2)
    descripció = models.TextField()
    marques = models.ManyToManyField(Marca, related_name='productes')

    def __str__(self):
        return self.nom


class Inventari(models.Model):
    id = models.AutoField(primary_key=True)
    tenda = models.ForeignKey(Tenda, related_name='inventaris', on_delete=models.CASCADE)
    producte = models.ForeignKey(Producte, related_name='inventaris', on_delete=models.CASCADE)
    quantitat = models.IntegerField()

    class Meta:
        unique_together = ('tenda', 'producte')

    def __str__(self):
        return f"{self.producte.nom} a {self.tenda.nom} - Quantitat: {self.quantitat}"