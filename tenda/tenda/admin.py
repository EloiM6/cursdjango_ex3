from django.contrib import admin

# Register your models here.

from .models import Tenda, Marca, Producte, Inventari
class TendaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nom', 'adreça')
    ordering = ("nom",)
    search_fields = ["nom"]
class MarcaAdmin(admin.ModelAdmin):
    list_display = ('id', 'nom')
    ordering = ("nom",)
    search_fields = ["nom"]
class ProducteAdmin(admin.ModelAdmin):
    list_display = ('id', 'nom', 'preu_unitari')
    filter_horizontal = ('marques',)  
    ordering = ("nom",)
    search_fields = ["nom"]

class InventariAdmin(admin.ModelAdmin):
    list_display = ('id', 'tenda', 'producte', 'quantitat')
    ordering = ("tenda","producte",)
    search_fields = ["tenda","producte"]
    
admin.site.register(Tenda, TendaAdmin)
admin.site.register(Marca, MarcaAdmin)
admin.site.register(Producte, ProducteAdmin)
admin.site.register(Inventari, InventariAdmin)
